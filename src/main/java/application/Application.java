package application;

import java.io.File;
import java.util.ArrayList;

import service.Manipulator;
import util.ScriptCreator;
import util.SourceUtil;
import enums.AsType;
import enums.Unit;

public class Application {

	public static void main(String[] args) {
		appendScript();
	}

	public static void appendScript() {

		Manipulator abManipulator = new Manipulator();

		File dir = new File(SourceUtil.class.getResource("/").getPath());
		ArrayList<String> fileList = SourceUtil.listFilesForFolder(dir);

		for (int i = 0; i < fileList.size(); i++) {
			String fn = fileList.get(i);
			String content;
			switch (fn) {
			case "bp-scom01.txt":
				content = abManipulator.generateScript(fn, null, null, null, AsType.MA, Unit.MB);
				createScript(fn, content);
				break;
			case "bp-scom02.txt":
				content = abManipulator.generateScript(fn, null, null, null, AsType.MA, Unit.MB);
				createScript(fn, content);
				break;
			case "pa-crm-file01.txt":
				content = abManipulator.generateScript(fn, null, null, null, AsType.MA, Unit.MB);
				createScript(fn, content);
				break;
			case "pa-crm-rep01.txt":
				content = abManipulator.generateScript(fn, null, null, null, AsType.MA, Unit.MB);
				createScript(fn, content);
				break;
			case "pa-crm01.txt":
				content = abManipulator.generateScript(fn, null, null, null, AsType.MA, Unit.MB);
				createScript(fn, content);
				break;
			case "pa-crm02.txt":
				content = abManipulator.generateScript(fn, null, null, null, AsType.MA, Unit.MB);
				createScript(fn, content);
				break;
			case "pa-scom01.txt":
				content = abManipulator.generateScript(fn, null, null, null, AsType.MA, Unit.MB);
				createScript(fn, content);
				break;
			case "pa-scom02.txt":
				content = abManipulator.generateScript(fn, null, null, null, AsType.MA, Unit.MB);
				createScript(fn, content);
				break;
			case "pa-sql02_mscrm_DB_allocated.txt":
				content = abManipulator.generateScript(fn, null, null, null, AsType.MA, Unit.MB);
				createScript(fn, content);
				break;
			case "pa-sql02_mscrm_DB_free_scape.txt.txt":
				content = abManipulator.generateScript(fn, null, null, null, AsType.MA, Unit.MB);
				createScript(fn, content);
				break;
			case "pa-sql02_Partner_MSCRM_allocated.txt":
				content = abManipulator.generateScript(fn, null, null, null, AsType.MA, Unit.MB);
				createScript(fn, content);
				break;
			case "pa-sql02_Partner_MSCRM_free_space.txt":
				content = abManipulator.generateScript(fn, null, null, null, AsType.MA, Unit.MB);
				createScript(fn, content);
				break;
			case "pa-sql02A.txt":
				content = abManipulator.generateScript(fn, null, null, null, AsType.MA, Unit.MB);
				createScript(fn, content);
				break;
			case "pa-sql02b.txt":
				content = abManipulator.generateScript(fn, null, null, null, AsType.MA, Unit.MB);
				createScript(fn, content);
				break;
			}
		}
	}

	public static void createScript(String fileName, String content) {
		ScriptCreator.createScriptFile(fileName, content);
	}
}
