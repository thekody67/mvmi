package model;

import java.util.Date;

public class TimeUnit {

	private Date time;

	private Double unit;

	public TimeUnit(Date time, Double unit) {
		super();
		this.time = time;
		this.unit = unit;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date date) {
		this.time = date;
	}

	public Double getUnit() {
		return unit;
	}

	public void setUnit(Double unit) {
		this.unit = unit;
	}

}
