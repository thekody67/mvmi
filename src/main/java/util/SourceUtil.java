package util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import model.TimeUnit;

import org.apache.commons.math3.analysis.interpolation.LinearInterpolator;
import org.apache.commons.math3.analysis.polynomials.PolynomialSplineFunction;

import enums.AsType;
import enums.Unit;

public class SourceUtil {

	private static final String df = "yyyy-MM-dd'T'HH:mm:ss";
	private static final String sqlDf = "yyyy.MM.dd HH:mm:ss";
	private static final String INSERT = "INSERT INTO [dbo].[MVMI_MACHINE_USAGE] ([MACHINE_ID],[CUSTER_ID],[BASE_SW_ID],[USAGE_TYPE],[TIME],[SIZE],[SIZE_UNIT]) VALUES";

	private static final Integer TIME = 1;
	private static final Integer WEIGHT = 2;

	public static ArrayList<TimeUnit> getManipulatedList(String fileName) {
		ArrayList<TimeUnit> list = readFileByName(fileName);
		ArrayList<TimeUnit> manipulatedList = new ArrayList<TimeUnit>();

		double[] xValues = new double[list.size()];
		double[] yValues = new double[list.size()];

		for (int i = 0; i < list.size(); i++) {
			xValues[i] = list.get(i).getTime().getTime();
			yValues[i] = list.get(i).getUnit();
		}

		LinearInterpolator lineInterpolator = new LinearInterpolator();
		PolynomialSplineFunction polynomialSplineFunction = lineInterpolator.interpolate(xValues, yValues);

		for (int i = 0; i < list.size(); i++) {

			Date startTime = list.get(i).getTime();
			Double startUnit = list.get(i).getUnit();

			TimeUnit tu = new TimeUnit(startTime, startUnit);
			manipulatedList.add(tu);

			if (i + 1 < list.size()) {

				Date endTime = list.get(i + 1).getTime();
				Calendar cal = Calendar.getInstance();
				cal.setTime(startTime);
				cal.add(Calendar.MINUTE, TIME);

				while (cal.getTime().before(endTime)) {

					Double polynomialPoint = polynomialSplineFunction.value(cal.getTimeInMillis());
					Double percent = (WEIGHT * polynomialPoint) / 100;
					Double minUnit = polynomialPoint - percent;
					Double maxUnit = polynomialPoint + percent;
					Double unit = minUnit + (new Random().nextDouble() * (maxUnit - minUnit));

					TimeUnit generatedTu = new TimeUnit(cal.getTime(), unit);
					manipulatedList.add(generatedTu);
					cal.add(Calendar.MINUTE, TIME);
				}
			}

		}
		return manipulatedList;
	}

	private static ArrayList<TimeUnit> readFileByName(String fileName) {
		File file = new File(SourceUtil.class.getResource("/" + fileName).getPath());
		ArrayList<TimeUnit> tmbList = readFile(file);
		return tmbList;
	}

	public static String createScript(Integer mId, Integer custerId, Integer baseId, Enum<AsType> type, Date time, Double size, Enum<Unit> unit) {
		return INSERT + "(" + mId + ", " + custerId + ", " + baseId + ", '" + type + "'," + " '" + sqlDateFormatter(time) + "', " + size + ",'"
				+ unit + "');\n";
	}

	// public static ArrayList<TimeUnit> readFolder() {
	// File dir = new File(SourceUtil.class.getResource("/").getPath());
	// ArrayList<String> fileList = listFilesForFolder(dir);
	// ArrayList<TimeUnit> tmbList = new ArrayList<TimeUnit>();
	// for (int i = 0; i < fileList.size(); i++) {
	// tmbList.addAll(readFileByPath(fileList.get(i)));
	// }
	// return tmbList;
	// }

	// private static ArrayList<TimeUnit> readFileByPath(String filepath) {
	// File file = new File(filepath);
	// ArrayList<TimeUnit> tmbList = readFile(file);
	// return tmbList;
	// }

	public static ArrayList<String> listFilesForFolder(final File folder) {
		ArrayList<String> fileList = new ArrayList<String>();
		for (final File fileEntry : folder.listFiles()) {
			if (fileEntry.isDirectory()) {
				listFilesForFolder(fileEntry);
			} else {
				if (fileEntry.getName().endsWith(".txt")) {
					fileList.add(fileEntry.getName());
				}
			}
		}
		return fileList;
	}

	private static Date stringToDate(String stringDate) {
		try {
			DateFormat formatter = new SimpleDateFormat(df);
			return formatter.parse(stringDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	private static String sqlDateFormatter(Date date) {
		DateFormat df = new SimpleDateFormat(sqlDf);
		return df.format(date);
	}

	private static Double stringToDoube(String stringDouble) {
		return Double.parseDouble(stringDouble);
	}

	private static ArrayList<TimeUnit> readFile(File file) {
		ArrayList<TimeUnit> tmbList = new ArrayList<TimeUnit>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));

			String line = br.readLine();
			while (line != null) {
				int sp = line.indexOf("|");
				Date time = stringToDate(line.substring(0, sp));
				Double unit = stringToDoube(line.substring(sp + 1, line.length()));

				TimeUnit tmb = new TimeUnit(time, unit);
				tmbList.add(tmb);

				line = br.readLine();
			}
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return tmbList;
	}
}
