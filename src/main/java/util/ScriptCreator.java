package util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

public class ScriptCreator {

	private static final String DIR = "scripts";

	public static void createScriptFile(String fileName, String content) {
		createDir();
		try {
			String fn = fileName.replace(".txt", ".sql");
			System.out.print("Create " + fn + " file...");
			PrintWriter writer = new PrintWriter(DIR + "\\" + fn, "UTF-8");
			writer.println(content);
			writer.close();
			System.out.print("Done\n");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

	private static void createDir() {
		File dir = new File("scripts");
		if (!dir.exists()) {
			System.out.println("creating directory: " + DIR + "...");
			boolean result = false;
			try {
				dir.mkdir();
				result = true;
			} catch (SecurityException se) {
				se.printStackTrace();
			}
			if (result) {
				System.out.println(DIR + " created");
			}
		}
	}
}
