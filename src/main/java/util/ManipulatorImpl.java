package util;

import enums.AsType;
import enums.Unit;

public interface ManipulatorImpl {

	public String generateScript(String fileName, Integer mId, Integer custerId, Integer baseId, Enum<AsType> type, Enum<Unit> unitType);

}
