package service;

import java.util.ArrayList;
import java.util.Date;

import model.TimeUnit;
import util.ManipulatorImpl;
import util.SourceUtil;
import enums.AsType;
import enums.Unit;

public class Manipulator extends SourceUtil implements ManipulatorImpl {

	@Override
	public String generateScript(String fileName, Integer mId, Integer custerId, Integer baseId, Enum<AsType> type, Enum<Unit> unitType) {
		ArrayList<TimeUnit> tu = getManipulatedList(fileName);
		StringBuilder sb = new StringBuilder();
		// sb.append(fileName + "\n\n");
		for (int i = 0; i < tu.size(); i++) {
			Date time = tu.get(i).getTime();
			Double unit = tu.get(i).getUnit();
			String sc = createScript(mId, custerId, baseId, type, time, unit, unitType);
			sb.append(sc);
		}
		return sb.toString();
	}

}
